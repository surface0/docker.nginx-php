# Description

This is a customized nginx:alpine base docker build files for a php(fpm) application.  

# Build

```
$ docker build --no-cache=true -t surface0/nginx:php-fpm .
```

# Environments

|name|default|example|
|---|---|---|
|NGINX_HOST|localhost|example.com|
|NGINX_PORT|80|8080|
|NGINX_ROOT|/data/app|/var/lib/www|
|NGINX_INDEX|index.html|index.php|
|NGINX_CLIENT_MAX_BODY_SIZE|2M|10M|
|NGINX_BASIC_AUTH|off|"basic authorization"|
|NGINX_BASIC_AUTH_USER_FILE|/dev/null|/data/.htpasswd|
|PHP_FPM_HOST|localhost|php-fpm|
|PHP_FPM_PORT|9000|9001|
|PHP_FPM_UPSTREAM|php-fpm|php-fpm2|
|PHP_INDEX|index.php|default.php|
|PHP_SCRIPT_NAME|\$fastcgi_script_name|index.php|
|PHP_SCRIPT_FILENAME|\$document_root\$fastcgi_script_name|/path/to/app.php|

# docker-compose.yml sample

```yml
version: '2'

services:
  nginx:
    image: surface0/nginx-alpine:php-fpm
    container_name: app1
    ports:
      - "8080:80"
    links:
      - php-fpm
    volumes:
      - ./data:/data:ro
    environment:
      PHP_FPM_HOST: "php-fpm"

  php-fpm:
    image: php:7.1-fpm-alpine
    volumes:
      - ./data:data:ro
```

# Additional Config

Put `nginx-appendix-conf` file into `/data`.

```
# CakePHP Application Example
location ~ ^/api/.+$ {
    fastcgi_pass $php_fpm_host:$php_fpm_port;
    fastcgi_intercept_errors on;
    include fastcgi_params;
    fastcgi_param SCRIPT_NAME $php_script_name;
    fastcgi_param SCRIPT_FILENAME $php_script_filename;
}

location / {
    try_files $uri $uri/ index.php?$args;
}
```
