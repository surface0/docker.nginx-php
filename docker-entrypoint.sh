#!/bin/sh
set -e

# copy appendix config
if [ -e /data/nginx-appendix-conf ]; then
    cp -f /data/nginx-appendix-conf /etc/nginx/conf.d/appendix
fi

# replace environments
envsubst ' \
        $$NGINX_HOST \
        $$NGINX_PORT \
        $$NGINX_ROOT \
        $$NGINX_INDEX \
        $$NGINX_CLIENT_MAX_BODY_SIZE \
        $$NGINX_BASIC_AUTH \
        $$NGINX_BASIC_AUTH_USER_FILE \
        $$PHP_FPM_HOST \
        $$PHP_FPM_PORT \
        $$PHP_INDEX \
        $$PHP_FPM_UPSTREAM \
        $$PHP_SCRIPT_NAME \
        $$PHP_SCRIPT_FILENAME \
    ' \
    < /etc/nginx/conf.d/default.conf.template \
    > /etc/nginx/conf.d/default.conf

exec "$@"
