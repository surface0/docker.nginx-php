FROM nginx:alpine

LABEL maintainer "surface0 <admin+docker@rainorshine.asia>"

ENV NGINX_HOST="localhost" \
    NGINX_PORT="80" \
    NGINX_ROOT="/data/app" \
    NGINX_INDEX="index.html" \
    NGINX_CLIENT_MAX_BODY_SIZE="2M" \
    NGINX_BASIC_AUTH="off" \
    NGINX_BASIC_AUTH_USER_FILE="/dev/null" \
    PHP_FPM_HOST="_" \
    PHP_FPM_PORT="9000" \
    PHP_FPM_UPSTREAM="php-fpm" \
    PHP_INDEX="index.php" \
    PHP_SCRIPT_NAME="\$fastcgi_script_name" \
    PHP_SCRIPT_FILENAME="\$document_root\$fastcgi_script_name"

COPY ./conf.d /etc/nginx/conf.d
COPY ./docker-entrypoint.sh /docker-entrypoint.sh

VOLUME /data

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
