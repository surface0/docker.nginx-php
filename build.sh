#!/bin/bash
set -ex;

rev=r2

docker build --no-cache=true -t surface0/nginx:php-fpm-${rev} -f ./Dockerfile .

echo Finished.
